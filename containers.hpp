/*
    This implementation has beed developed and used as part of the work described in 
    Fast and portable text detection, F. Odone L. Zini
    Submitted to Machine Vision and Application
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTAINERS_HPP
#define CONTAINERS_HPP

#include <vector>
#include <stdlib.h>

template<class T, int N_LEVELS, int LEVEL_SIZE>
class PriorityQueue{
public:
    PriorityQueue() :emptyMap(4,0){
    }

    void setFull(unsigned int priority){
        const int level = priority / LEVEL_SIZE;
        const int pos = priority % LEVEL_SIZE;
        emptyMap[level] |= 1l << pos;
    }

    void setEmpty(unsigned int priority){
        const unsigned int level = priority / LEVEL_SIZE;
        const unsigned int pos = priority % LEVEL_SIZE;
        emptyMap[level] &= ~(1l << pos);
    }

    int nextIndex() const{
        int index = -1;
        if(( index =  __builtin_ffsl(emptyMap[0])) != 0){
            return index - 1;
        }else if(( index =  __builtin_ffsl(emptyMap[1])) != 0){

            return index +64 -1;
        }else if(( index =  __builtin_ffsl(emptyMap[2])) != 0){

            return index + 128 -1;

        }else if(( index =  __builtin_ffsl(emptyMap[3])) != 0){
            return index + 192 -1;
        }
        return -1;
    }

private:
    std::vector<unsigned long int> emptyMap;
};


template <class T>
class StaticSizeVector{
public:
    StaticSizeVector(int siz = (768*432)){
        data = (T*)calloc(sizeof(T), siz);
        last = -1;
    }

    void clear(){
        last = -1;
    }

    bool empty() const {
        return last == -1;
    }

    void push_back(const T& v){
        last++;
        *(data + last) = v;
    }

    bool pop_back(){
        if(last >= 0){
            last--;
        }
        return last >=  0;
    }

    T& back(){
        return data[last];
    }

private:

    int last;
    T* data;
};


template <class T>
class ArrayListVector{
public:
    ArrayListVector(int blockSize): blockSize(blockSize){
        list.push_back((T*)calloc(sizeof(T), blockSize));
        blockIndex = 0;
        innerIndex = -1;
    }

    void push_back(const T& v) {
        innerIndex++;
        if(innerIndex < blockSize){
            *(list[blockIndex] + innerIndex) = v;
        }else{
            list.push_back((T*)calloc(sizeof(T), blockSize));
            innerIndex = 0;
            blockIndex++;
            *(list[blockIndex] + innerIndex) = v;
        }
    }

    void pop_back(){
        innerIndex--;
        if(innerIndex < 0){
            innerIndex = 0;
            blockIndex--;
            if(blockIndex < 0){
                blockIndex = 0;
                innerIndex = -1;
            }
        }
    }

    T& back(){
        return *(list[blockIndex] + innerIndex);
    }

    void clear(){
        blockIndex= 0;
        innerIndex = -1;
    }

    inline int size() const{
        int s = blockIndex * blockSize + innerIndex;
        return s >= 0 ? s : 0;
    }

    inline bool empty()const{
        return innerIndex < 0;
    }

    int blockIndex;
    int innerIndex;
    const int blockSize;
    std::vector<T*> list;

};

template <class T>
class Pool{
public:
    Pool(int blockSize): blockSize(blockSize){
        list.push_back((T*)calloc(sizeof(T), blockSize));
        blockIndex = 0;
        innerIndex = 0;
    }

    T* nextAddress() {
        if(innerIndex < blockSize){
            return (list[blockIndex] + innerIndex++);
        }else{
	    if((int)list.size() <= blockIndex + 1){
                list.push_back((T*)calloc(sizeof(T), blockSize));
            }
            innerIndex = 0;
            blockIndex++;
            return (list[blockIndex] + innerIndex++);
        }
    }

    inline int size() const{
        return blockIndex * blockSize + innerIndex;
    }

    void clear(){
        blockIndex = 0;
        innerIndex = 0;
    }

    class iterator{

    public:
        iterator(int b, int i, int blockSize, std::vector<T*> list):list(list){
            bi = b;
            ii = i;
            this->blockSize = blockSize;
        }

        void next(){
            ii++;
            if(ii >= blockSize){
                ii = 0;
                bi++;
            }
        }

        bool operator<=(const iterator& b){
            return bi < b.bi || (bi == b.bi &&  ii <= b.ii);
        }

        T& operator*(){
            return *((list[bi] + ii));
        }

        int bi;
        int ii;
        int blockSize;
        std::vector<T*> list;
    };

    Pool<T>::iterator begin(){
        return iterator(0, 0, blockSize, list);
    }

    Pool<T>::iterator end(){
        return iterator(blockIndex, innerIndex, blockSize, list);
    }

    int blockIndex;
    int innerIndex;
    const int blockSize;
    std::vector<T*> list;
};


#endif // CONTAINERS_HPP
